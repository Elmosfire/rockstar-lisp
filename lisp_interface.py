

class Lisp:
    @staticmethod
    def assign_variable(name, value):
        return f"(setq {name} {value})"