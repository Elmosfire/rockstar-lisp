import parsec
from functools import reduce
from itertools import chain, groupby

    
        

class Tokenizer:
    keywordset = set()
    @classmethod
    def annotate_keyword(cls, word):
        if word in cls.keywordset:
            return f"&{word}"
        else:
            return f"!{word}"
    @classmethod
    def annotate_keywords_combine(cls, words):
        return ''.join(cls.annotate_keywords(words))
    
def extract_keyword_list(expr):
    yield from (x.replace("_", " ") for x in expr[2:-1].split(','))
    

with open("rules.txt") as file:
    for string in file:
        words = string.strip().split(' ')
        for x in words:
            word = x.strip()
            if word.startswith('$[') and word.endswith(']'):
                names = extract_keyword_list(word)
                for name in names:
                    Tokenizer.keywordset.add(name)



Tokenizer.parse_literal = parsec.regex("[A-Za-z0-9'-]+").desc("word")

@parsec.generate
def parse_keywords():
    res = yield reduce(parsec.try_choice, [parsec.string(keyword) for keyword in Tokenizer.keywordset])
    return res

Tokenizer.parse_literal_or_keyword = parsec.try_choice(parse_keywords,Tokenizer.parse_literal).parsecmap(Tokenizer.annotate_keyword)
Tokenizer.parse_literal_or_keyword_with_comma = (Tokenizer.parse_literal_or_keyword + parsec.string(",").result("%%"))
Tokenizer.parse_literal_or_keyword_any = Tokenizer.parse_literal_or_keyword_with_comma ^ Tokenizer.parse_literal_or_keyword.parsecmap(lambda x:(x,))
Tokenizer.parse_comment = parsec.regex(r"[\t ]*\(.*?\)[\t ]*").desc("any string in brackets")
Tokenizer.parse_spacing = parsec.regex("[\t ]+").desc("space or tab")
Tokenizer.parse_seperator = Tokenizer.parse_comment ^ Tokenizer.parse_spacing
Tokenizer.parse_line = Tokenizer.parse_seperator >> parsec.sepBy1(Tokenizer.parse_literal_or_keyword_any, Tokenizer.parse_seperator) << Tokenizer.parse_seperator

def tokenise_line_raw(line):
    return (list(chain(*Tokenizer.parse_line.parse_strict(" " + line.strip() + " "))))

#def expand_line(line):
#    line, *alternates = [list(y) for x,y in groupby(line,"%%".__ne__) if x]
#    print(line, alternates)
#    head = line[:-1]
#    for alt in [[line[-1]]] + alternates:
#        yield head + alt

def tokenise_line(line):
    yield ",".join(tokenise_line_raw(line)) + ","