from parsec import Parser
import parsec
from functools import reduce
from pathlib import Path
from functools import partial
from itertools import chain
from tokeniser import tokenise_line, extract_keyword_list

@parsec.generate
def parse_wrapper():
    x = yield VariableParser.parse_line_with_any(parserules)
    return x

class VariableParser:
    @classmethod
    def keyword_helper(cls, name):
        return parsec.string(f"&{name},").result(name).desc(name)
    
    @classmethod
    def keywords(cls, names):
        return reduce(parsec.try_choice, map(cls.keyword_helper,[x.strip().replace("_", " ") for x in names])).desc(f"any of {','.join(names)}")
    @classmethod
    def variable_helper(cls):
        return parsec.regex(f"![A-Za-z0-9'-]+,").parsecmap(lambda x:x[1:-1]).desc("word for variable")
    
    @classmethod
    def variable_combines(cls, vars):
        return "_".join(vars)
    
    @classmethod
    def variable(cls):
        return parsec.many1(cls.variable_helper()).parsecmap(cls.variable_combines).desc("variable name")
    
    @staticmethod
    def get_value_poetic_literal(literal):
        return str(sum(1 for x in literal if x != "'"))
    
    @classmethod
    def poetic_literal_helper(cls):
        return parsec.regex(f"[!&][A-Za-z0-9'-]+,").parsecmap(lambda x:x[1:-1]).parsecmap(cls.get_value_poetic_literal).desc("word for poetic literal")
        
    @classmethod
    def poetic_literal(cls):
        return (parsec.many1(cls.poetic_literal_helper()).parsecmap(''.join) ^ parsec.regex("[0-9]+")).desc("poetic literal")
    
    
    @staticmethod
    def parse_line_with_any(parsers):
        return reduce(parsec.try_choice, parsers)
        
    
    @classmethod
    def expression(cls):
        return parse_wrapper
    
    @classmethod
    def expression_or_variable(cls):
        return Aritmatic.do_math_expression().desc("expression")
    
class Aritmatic:
    all_tokens = []
    @classmethod
    def parse_expression(cls, parser, keywords, lispfun):
        return ((parser << VariableParser.keywords(keywords)) + parser).parsecmap(lambda x:"({} {} {})".format(lispfun, *x)).desc(lispfun)
        
    @classmethod
    def do_math_expression(cls):
        main_parser = parse_wrapper ^ VariableParser.variable()
        
        for keywords, lispfun in Aritmatic.all_tokens:
            print(keywords, lispfun)
            main_parser =  cls.parse_expression(main_parser, keywords, lispfun) ^ main_parser
            
        return main_parser
    
    @classmethod 
    def list_constant(cls):
        list_seperator = parsec.string('%%') ^ VariableParser.keywords('and') ^ (parsec.string('%%') << VariableParser.keywords('and')) 
        return parsec.sepBy1(Aritmatic.do_math_expression(), list_seperator).desc("list seperated by , or and").parsecmap(lambda x:"(list {})".format(" ".join(x)))
    
    @classmethod 
    def list_literal(cls):
        list_seperator = parsec.string('%%') ^ VariableParser.keywords('and') ^ (parsec.string('%%') << VariableParser.keywords('and')) 
        return parsec.sepBy1(Aritmatic.do_math_expression(), list_seperator).desc("list seperated by , or and").parsecmap(lambda x:"({})".format(" ".join(x)))
    
def parse_as_lisp_interface(dict_parser, formatter):
    def formatter_combiner(values):
        value_dict = dict(zip(dict_parser, values))
        print(value_dict)
        return formatter.format(**value_dict)
    return parsec.joint(*dict_parser.values()).parsecmap(formatter_combiner)
    
def lisp_parser_from_string(string):
    rockstar, lisp = string.split("->")
    rockstar = rockstar.strip().split(" ")
    lisp = lisp.strip()
    dict_parser = {}
    for x in rockstar:
        ckey = x[0]
        name = x[1:]
        assert name not in dict_parser
        if ckey == '.':
            dict_parser[name] = VariableParser.variable()
        if ckey == '$':
            assert name.startswith('[') and name.endswith(']'), f"{name} is invalidly formatted"
            names = list(extract_keyword_list(x))
            dict_parser[names[0]] = VariableParser.keywords(names)
        if ckey == '_':
            dict_parser[name] = VariableParser.poetic_literal()
        if ckey == '=':
            dict_parser[name] = VariableParser.expression_or_variable()
        if ckey == '%':
            dict_parser[name] = Aritmatic.list_constant()
        if ckey == '~':
            dict_parser[name] = Aritmatic.list_literal()
    return parse_as_lisp_interface(dict_parser, lisp)

def lisp_parser_aritmatic_from_string(string):
    rockstar, lisp = string.split("+>")
    rockstar = rockstar.strip()
    lisp = lisp.strip()
    ckey = rockstar[0]
    name = rockstar[1:]
    if ckey == '$':
        assert name.startswith('[') and name.endswith(']'), f"{name} is invalidly formatted"
        names = list(extract_keyword_list(rockstar))
        return (names, lisp)

with open("rules.txt") as file:
    for x in file:
        line = x.strip()
        if "+>" in line:
            Aritmatic.all_tokens.append(lisp_parser_aritmatic_from_string(line))
            print(Aritmatic.all_tokens)
            
with open("rules.txt") as file:
    parserules = []
    lineonly_parse_rules = {}
    for x in file:
        line = x.strip()
        if "->" in line:
            parserules.append(lisp_parser_from_string(line))
        if "=>" in line:
            lineonly_parse_rules[line] = lisp_parser_from_string(line.replace("=>", '->'))
            
print(lineonly_parse_rules)

def parse_any_and_compile_report(tokenized, parsers):
    try:
        return VariableParser.parse_line_with_any(parsers.values()).parse_strict(tokenized)
    except parsec.ParseError:
        for name, parser in parsers.items():
            try:
                parser.parse_strict(tokenized)
            except parsec.ParseError as e:
                print(name)
                print("expected: ", e.expected)
                #print("tokens: ", e.text)
            else:
                raise AssertionError("parser worked?")
        raise AssertionError("parser failed")
    
def parse_rockstar_lines(lines):
    for line in lines.split("\n"):
        if not line.strip():
            yield ''
            continue
        for tokenized in tokenise_line(line.strip()):
            print(">>", tokenized)
            
            yield parse_any_and_compile_report(tokenized, lineonly_parse_rules)
    
# print(parse_rockstar_line("life is like a(this works)box of chocolate's (more comments)"))

with open("test.rock") as file:
    with open("test.lisp", "w") as file2:
        for x in parse_rockstar_lines(file.read()):
            print("WRITING:", x)
            file2.write(x + '\n')

#print(parsec.many1(VariableParser.poetic_literal_helper()).parsecmap(''.join).parse_strict("!test,!test2,!test3,"))